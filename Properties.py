"""
MCLabUtils' Properties file reader into a Python dict
"""

from collections import deque
import re
import logging
import json

from .Debug import _indent 

def Properties2dict(properties_fname):
    nested_properties_stack = deque() # stack of (key, dict) elements
    nested_properties_stack.append( ("", dict() ) )
    
    depth = 0

    with open(properties_fname, "r") as file:
        line_no = 0
        for line in file:
            line_no += 1
            line = line[:line.find('#')].strip()
            #logging.debug(f"{_indent(depth)}Line {line_no}: '{line}'")

            if (line == ""):
                #logging.debug(f"{_indent(depth)} - empty line -> skipped")
                continue
            elif (line == "}"):             
                _key, nested_properties = nested_properties_stack.pop()
                #logging.debug(f"{_indent(depth)} - end nested properties '{_key}'")
                (_, current_layer) = nested_properties_stack[-1]
                current_layer[_key] = nested_properties
                depth = depth - 1
                
            else:
                matches = re.match("([^\t ]*)[\t ]+([^=]+)=(.*)$", line.strip())
                # matches[1]: type (to be stripped)
                # matches[2]: key name (to be stripped)
                # matches[3]: value or '{' (to be stripped)
                _type = matches[1].strip().lower()
                _key = matches[2].strip()
                _value = matches[3].strip()

                #logging.debug(f"{_indent(depth)} - type = '{_type}', key = '{_key}', value = '{_value}'")
                if (_type == "properties" or (_type == "" and _value == "{")):
                    nested_properties_stack.append( (_key, dict()) )
                    depth = depth + 1
                else:
                    (_, current_layer) = nested_properties_stack[-1]
                    if (_type in ["int", "uint", "long", "ulong"]):
                        current_layer[_key] = int(_value)
                    elif (_type in ["double", "numeric"]):
                        current_layer[_key] = float(_value)
                    else:
                        current_layer[_key] = str(_value)

    #logging.debug(f"All done: len(nested_properties_stack) == {len(nested_properties_stack)} (should be 1)")
    (_, result) = nested_properties_stack.pop()
    return result


if __name__ == "__main__":
    import sys
    logging.basicConfig(level=logging.DEBUG)
    fname = sys.argv[1]
    logging.info("Parsing Properties file: %s", fname)
    _dict = Properties2dict(fname)

    logging.info(f"Parsed json:\n{json.dumps(_dict, indent=2)}")
    

          